package ProyectoUF1Bueno;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.*;  

@XmlRootElement
public class Alumne implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nom;
	private String DNI;
	private String adreca;
	private ArrayList<String> telefons = new ArrayList<String>();
	private String mail;
	
	public Alumne() {
		super();
	}

	public Alumne(String nom, String dNI, String adreca, ArrayList<String> telefons, String mail) {
		super();
		this.nom = nom;
		DNI = dNI;
		this.adreca = adreca;
		this.telefons = telefons;
		this.mail = mail;
	}

	@XmlElement
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@XmlElement
	public String getDNI() {
		return DNI;
	}
	
	public void setDNI(String dNI) {
		DNI = dNI;
	}
	
	@XmlElement
	public String getAdreca() {
		return adreca;
	}
	
	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}
	
	@XmlElementWrapper(name = "telefons")
	@XmlElement(name = "telefon")
	public ArrayList<String> getTelefons() {
		return telefons;
	}
	
	public void setTelefons(ArrayList<String> telefons) {
		this.telefons = telefons;
	}
	
	@XmlElement
	public String getMail() {
		return mail;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}	
	
}
