package ProyectoUF1Bueno;

import java.io.Serializable;

public class Maquina implements Serializable{

	
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private String aula;
		private String nom;
		private String processador;
		private boolean grafica;
		
		public Maquina() {
			super();
		}

		public Maquina(String aula, String nom, String processador, boolean grafica) 
		{
			super();
			this.aula = aula;
			this.nom = nom;
			this.processador = processador;
			this.grafica = grafica;
		}
		
		public String getAula() 
		{
			return aula;
		}

		public void setAula(String aula) 
		{
			this.aula = aula;
		}

		public String getNom() 
		{
			return nom;
		}

		public void setNom(String nom) 
		{
			this.nom = nom;
		}

		public String getProcessador() 
		{
			return processador;
		}

		public void setProcessador(String processador) 
		{
			this.processador = processador;
		}

		public boolean isGrafica() 
		{
			return grafica;
		}

		public void setGrafica(boolean grafica) 
		{
			this.grafica = grafica;
		}
		
		
		
		
		
		
		
		
}
