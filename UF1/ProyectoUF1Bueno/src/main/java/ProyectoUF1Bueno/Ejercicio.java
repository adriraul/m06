package ProyectoUF1Bueno;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Ejercicio {

	public static void main(String[] args) {
		
		/*
    	 * Probamos que funcionan todas las lecturas y escrituras
 
		ArrayList<String> professors = new ArrayList<String>();
		Institut institut = null;
		ArrayList<Aula> aules = new ArrayList<Aula>();
 
    	professors = llegirTxt();
    	escriureTxt(professors);  
    	
    	institut = llegirXml();
    	escriureXml(institut);
    	
    	aules = llegirJSON();
    	
    	System.out.println(aules.get(0).getCapacitat());
    	escriureJSON(aules);
    	*/
		
		//AfegimProfessor
		afegirProfessor("Bravo Sánchez, Adrián");
		
		//JubilemProfessor
		jubilarProfessor("Bravo Sánchez, Adrián");
		
		//AfegimAlumne
		ArrayList<String> telsAl = new ArrayList<String>();
		
		String tel1 = "662612987";
		String tel2 = "937178317";
		
		telsAl.add(tel1);
		telsAl.add(tel2);
		
		Alumne al = new Alumne("Adrián Bravo Sánchez", "48268653W", "Avd/ Matadepera, 231, 4º2ª", telsAl, "adrianbravosanchez@gmail.com");
		
		afegeixAlumne(al);
		
		//AfegimTElefon
		afegeixTelefon("28923792Z", "666666666");
        
		//EliminemAlumnes
		alCarrer("28923792Z");
		
		//AfegirMàquina
		comprarMaquina("1.6", "Maquina de la NASA", "i9 5600K", true);
		
		//CanviemMàquina
		canviaMaquina("1-6-1", "C4");
		
		//SwitchArie
		switchAC("C4");
		
		//Crear una classe
		crearClasse();
		
		//Leemos clase y printamos
		llegirClasse("classe.dat");

	}

	private static void llegirClasse(String string) 
	{
		
		try
		{
			FileInputStream fis = new FileInputStream("classe.dat");
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			Classe classe = new Classe();
			
			classe = (Classe) ois.readObject();	
			
			System.out.println("Nom del professor: " + classe.getProfessor());
			System.out.println("Nom de l'aula: " + classe.getAula().getNom());
			System.out.println("Capacitat de la classe: " + classe.getAula().getCapacitat());
			System.out.println("Aire acondicionat: " + classe.getAula().isAireacondicionat());
			
			ArrayList<Maquina> maquines = new ArrayList<Maquina>();
			int i = 1;
			
			maquines = classe.getAula().getMaquines();
			
			for(Maquina m : maquines)
			{
				System.out.println("Nom de la màquina " + i + ": " + m.getNom());
				i++;
			}
			
			ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
			
			alumnes = classe.getAlumnes();
			
			i = 1;
			
			for(Alumne a : alumnes)
			{
				System.out.println("Alumne " + i + ": " + a.getNom() + " amb DNI " + a.getDNI());
				i++;
			}

			
		}catch(Exception e)
		{
			//e.printStackTrace();
		}

	}

	private static void crearClasse() 
	{
		//Obtenim un aula
		ArrayList<Aula> aules = new ArrayList<Aula>();
		Aula aula = new Aula();
		
		aules = llegirJSON();
    	aula = aules.get(0);
    	
    	//Obtenim un professor
		ArrayList<String> professors = new ArrayList<String>();
    	String professor;
    	
		professors = llegirTxt();
		professor = professors.get(0);
		
		//Obtenim 5 alumnes aleatoris
		ArrayList<Integer> aleatoris = new ArrayList<Integer>();
		Institut institut = null;
		ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
		ArrayList<Alumne> alumnes1 = new ArrayList<Alumne>();
		
		institut = llegirXml();
		alumnes = institut.getAlumnes();
		int numAlumnes = alumnes.size();
		int num;
		
		boolean trobat;
		
		while(aleatoris.size() <= 5)
		{
			num = (int) (Math.random()*numAlumnes);
			trobat = false;
			
			for(int a : aleatoris)
			{
				if(a == num)
				{
					trobat = true;
				}
			}
			
			if(trobat == false)
			{
				aleatoris.add(num);
			}			
		}
		
		for(int i = 0; i < aleatoris.size(); i++)
		{
			alumnes1.add(alumnes.get(aleatoris.get(i)));
		}
		
		Classe classe = new Classe(aula, professor, alumnes1);
		
		try
		{
			FileOutputStream fos = new FileOutputStream("classe.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(classe);					
			
		}catch(Exception e)
		{
			//e.printStackTrace();
		}

	}

	private static void switchAC(String string) 
	{
		ArrayList<Aula> aules = new ArrayList<Aula>();
		
		aules = llegirJSON();
		
		for(Aula a : aules)
		{
			if(a.getNom().equals(string))
			{
				if(a.isAireacondicionat())
				{
					a.setAireacondicionat(false);
				}else
				{
					a.setAireacondicionat(true);
				}
			}
		}
		
		escriureJSON(aules);	
	}

	private static void canviaMaquina(String string, String string2) 
	{
		//AVISO, ES SUPER POCO ÓPTIMO, TIENE 5 FOR'S XD
		Maquina ma = new Maquina();
		String claseantigua = "";
		
		ArrayList<Aula> aules = new ArrayList<Aula>();
		ArrayList<Maquina> maquines = new ArrayList<Maquina>();
		
		aules = llegirJSON();
		
		//Localizamos la maquina y la introducimos en la auxiliar "ma"
		for(Aula a : aules)
		{
			maquines = a.getMaquines();
			
			for(Maquina m : maquines)
			{
				if(m.getNom().equals(string))
				{
					ma.setNom(m.getNom());
					ma.setAula(string2);
					ma.setGrafica(m.isGrafica());
					ma.setProcessador(m.getProcessador());
					claseantigua = a.getNom();
				}
			}		
		}
		
		
		//Buscamos la nueva clase y añadimos la maquina
		for(Aula a : aules)
		{
			if(a.getNom().equals(string2))
			{
				maquines = a.getMaquines();
				maquines.add(ma);
				a.setMaquines(maquines);
			}
		}
		
		//Buscamos la clase anterior y la borramos
		for(Aula a : aules)
		{
			if(a.getNom().equals(claseantigua))
			{
				maquines = a.getMaquines();
				
				for (int i = 0; i < maquines.size(); i++)
				{
					if(maquines.get(i).getNom().equals(string))
					{
						maquines.remove(i);
					}
				}
				
				a.setMaquines(maquines);
			}
			
		}
			
		escriureJSON(aules);	
	}

	private static void comprarMaquina(String string, String string2, String string3, boolean b) 
	{
		ArrayList<Aula> aules = new ArrayList<Aula>();
		ArrayList<Maquina> maquines = new ArrayList<Maquina>();
		
		aules = llegirJSON();
		
		for(Aula a : aules)
		{
			if(a.getNom().equals(string))
			{
				maquines = a.getMaquines();
				Maquina m = new Maquina(string, string2, string3, b);
				maquines.add(m);
				a.setMaquines(maquines);				
			}
		}
		
		escriureJSON(aules);
		
	}

	private static void alCarrer(String string) 
	{
		Institut i;
		ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
		int alumnetrobat;
		i = llegirXml();
		
		alumnes = i.getAlumnes();

		for(int x = 0; x < alumnes.size(); x++)
		{
			if(alumnes.get(x).getDNI().equals(string))
			{
				alumnes.remove(x);
			}
		}
		
		escriureXml(i);	
		
	}

	private static void afegeixTelefon(String string, String string2) 
	{
		Institut i;
		ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
		ArrayList<String> telefons = new ArrayList<String>();
		
		i = llegirXml();
		
		alumnes = i.getAlumnes();
		
		for(Alumne a : alumnes)
		{
			if(a.getDNI().equals(string))
			{
				telefons = a.getTelefons();
				telefons.add(string2);
				a.setTelefons(telefons);
			}
		}
		
		escriureXml(i);	
		
	}

	private static void afegeixAlumne(Alumne al) 
	{
		Institut i;
		ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
		
		i = llegirXml();
		
		alumnes = i.getAlumnes();
		alumnes.add(al);
		
		escriureXml(i);	
		
	}

	private static void jubilarProfessor(String string) 
	{		
		ArrayList<String> professors = new ArrayList<String>();
		
		professors = llegirTxt();
		
		professors.remove(string);
		
		Collections.sort(professors);
		
		escriureTxt(professors);		
	}

	private static void afegirProfessor(String string) 
	{
		
		ArrayList<String> professors = new ArrayList<String>();
		
		professors = llegirTxt();
		
		professors.add(string);
		Collections.sort(professors);
		
		escriureTxt(professors);	
		
	}

	private static ArrayList<String> llegirTxt() 
	{	 
		ArrayList<String> professors = new ArrayList<String>();
		try 
	      {
	         File f = new File ("professors.txt");
	         FileReader fr = new FileReader (f);
	         BufferedReader br = new BufferedReader(fr);
	         
	         while(br.ready())
	         {		         
		         professors.add(br.readLine());
	         } 
	         
	         return professors;
	      }
	      catch(Exception e){
	         e.printStackTrace();
	      }	
		
		return professors;
		
	}
	
	private static void escriureTxt(ArrayList<String> professors)
	{
		try
		{
			FileWriter fw = new FileWriter("professorsExercici.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(String s : professors)
			{
				bw.write(s + "\n");
			}
			
			bw.flush();
			bw.close();
			fw.close();			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private static Institut llegirXml()
	{
		Institut institut = null;
		
		try {  
			   
	        File file = new File("alumnes.xml");  
	        JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);  
	        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
	        institut= (Institut) jaxbUnmarshaller.unmarshal(file);  
	          	       
	        return institut;
	        
	      } catch (Exception e) {  
	        e.printStackTrace();  
	      }  
		
		return institut;
	}
	
	private static void escriureXml(Institut institut)
	{
		try {
			
			FileOutputStream fos = new FileOutputStream("alumnesExercici.xml");
			JAXBContext contextObj = JAXBContext.newInstance(Institut.class);
			Marshaller marshallerObj = contextObj.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");			
			marshallerObj.marshal(institut, fos);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static ArrayList<Aula> llegirJSON()
	{
		ArrayList<Aula> aules = new ArrayList<Aula>();
		
		Aula aula1 = new Aula();
		Maquina maquina1 = new Maquina();
		ArrayList<Maquina> maquines1 = null;

        try {
        
        	JSONParser parser = new JSONParser();

			Object obj = parser.parse(new FileReader("aules.json"));
			
			JSONArray arrayAules = (JSONArray) obj;

			for(Object o : arrayAules) 
			{				
				aula1 = new Aula();
				maquines1 = new ArrayList<Maquina>();				
				
				JSONObject aula = (JSONObject) o;
				
				aula1.setNom((String)aula.get("nom"));
				aula1.setCapacitat((Long)aula.get("capacitat"));
				aula1.setAireacondicionat((Boolean)aula.get("aireacondicionat"));
				
				JSONArray maquines = (JSONArray) aula.get("maquines");

				for(Object o2 : maquines) 
				{
					maquina1 = new Maquina();
					JSONObject maquina = (JSONObject) o2;

					maquina1.setNom((String)maquina.get("nom"));
					maquina1.setProcessador((String)maquina.get("processador"));
					maquina1.setGrafica((Boolean)maquina.get("grafica"));
					
					maquines1.add(maquina1);
				}
				
				aula1.setMaquines(maquines1);
				aules.add(aula1);
				
			}			
			return aules;
			}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return aules;
	}
	
	private static void escriureJSON(ArrayList<Aula> aules)
	{
		JSONArray aules1 = new JSONArray();
		JSONArray maquines1 = null;
		JSONObject aula1 = null;
		JSONObject maquina1 = null;

		
		for(Aula a : aules)
		{
			aula1 = new JSONObject();
			
			aula1.put("nom", a.getNom());
			aula1.put("capatitat", a.getCapacitat());
			aula1.put("aireacondicionat", a.isAireacondicionat());
			
			maquines1 = new JSONArray();
			
			for(Maquina m : a.getMaquines())
			{
				maquina1 = new JSONObject();
				
				maquina1.put("nom", m.getNom());
				maquina1.put("processador", m.getProcessador());
				maquina1.put("grafica", m.isGrafica());
				
				maquines1.add(maquina1);				
			}
			
			aula1.put("maquines", maquines1);
			
			aules1.add(aula1);
		}
	
		 try {
			 
			FileWriter file = new FileWriter("AulesExercici.json");

			 file.write(aules1.toJSONString());
			 file.flush();
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
	}
}
