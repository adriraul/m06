package ProyectoUF1Bueno;

import java.util.ArrayList;
import javax.xml.bind.annotation.*;  

@XmlRootElement(name = "institut")
public class Institut {
	
	private String nom;
	private ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
	
	
	public Institut() {
		super();
	}

	public Institut(String nom, ArrayList<Alumne> alumnes) 
	{
		super();
		this.nom = nom;
		this.alumnes = alumnes;
	}
	
	@XmlElement
	public String getNom() 
	{
		return nom;
	}

	public void setNom(String nom) 
	{
		this.nom = nom;
	}
	
	@XmlElementWrapper(name = "alumnes")
	@XmlElement(name = "alumne")
	public ArrayList<Alumne> getAlumnes() 
	{
		return alumnes;
	}

	public void setAlumnes(ArrayList<Alumne> alumnes) 
	{
		this.alumnes = alumnes;
	}
	
	

	
	
}
