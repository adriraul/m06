package ProyectoUF1Bueno;

import java.io.Serializable;
import java.util.ArrayList;

public class Classe implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Aula aula;
	private String professor;
	private ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
	
	public Classe(Aula aula, String professor, ArrayList<Alumne> alumnes) 
	{
		super();
		this.aula = aula;
		this.professor = professor;
		this.alumnes = alumnes;
	}
	
	public Classe() {
		super();
	}



	public Aula getAula() 
	{
		return aula;
	}

	public void setAula(Aula aula) 
	{
		this.aula = aula;
	}

	public String getProfessor() 
	{
		return professor;
	}

	public void setProfessor(String professor) 
	{
		this.professor = professor;
	}

	public ArrayList<Alumne> getAlumnes() 
	{
		return alumnes;
	}

	public void setAlumnes(ArrayList<Alumne> alumnes) 
	{
		this.alumnes = alumnes;
	}

	public static long getSerialversionuid() 
	{
		return serialVersionUID;
	}
	
	
	
	
}
