package examenUF1.examenUF1;

import java.io.Serializable;

public class Gat implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nom;
	private String raza;
	private int edat;
	private String menjarPreferit;
	boolean capat;
	
	public Gat(String nom, String raza, int edat, String menjarPreferit, boolean capat) 
	{
		super();
		this.nom = nom;
		this.raza = raza;
		this.edat = edat;
		this.menjarPreferit = menjarPreferit;
		this.capat = capat;
	}

	public Gat() 
	{
		super();
	}

	public String getNom() 
	{
		return nom;
	}

	public void setNom(String nom) 
	{
		this.nom = nom;
	}

	public String getRaza() 
	{
		return raza;
	}

	public void setRaza(String raza) 
	{
		this.raza = raza;
	}

	public int getEdat() 
	{
		return edat;
	}

	public void setEdat(int edat) 
	{
		this.edat = edat;
	}

	public String getMenjarPreferit() 
	{
		return menjarPreferit;
	}

	public void setMenjarPreferit(String menjarPreferit) 
	{
		this.menjarPreferit = menjarPreferit;
	}

	public boolean isCapat() 
	{
		return capat;
	}

	public void setCapat(boolean capat) 
	{
		this.capat = capat;
	}	
}
