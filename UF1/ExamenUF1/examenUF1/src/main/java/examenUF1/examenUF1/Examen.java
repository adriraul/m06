package examenUF1.examenUF1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class Examen {

	public static void main(String[] args) {
		
		//EJERCICIO1		
		ArrayList<String> gats = new ArrayList<String>();
		ArrayList<Gat> gats1 = new ArrayList<Gat>();
		
		//LEO FICHERO
		gats = llegirTxt();
		
		Gat gg;
		
		//PASO FICHERO A CLASE
		for(String g : gats)
		{
			gg = new Gat();
			
			String[] parts = g.split("-");
			
			gg.setNom(parts[0]);
			gg.setRaza(parts[1]);
			gg.setEdat(Integer.parseInt(parts[2]));
			gg.setMenjarPreferit(parts[3]);
			gg.setCapat(Boolean.parseBoolean(parts[4]));
			
			gats1.add(gg);
		}
		
		//PRUEBA DE IMPRESIÓN DE NOMBRES A TRABÉS DE CLASE GATO
		/*for(Gat g : gats1)
		{
			System.out.println(g.getNom());
		}*/
		
		Serializar(gats1);
		
		//EJERCICIO 2
		//CREO INSTITUTO, LO LEO Y MUESTRO LOS PROFESORES
		Institut institut = null;
		
		institut = llegirXml();
		
		for(Professor p : institut.getProfessors())
		{
			System.out.println(p.getNom());
		}
		
		//UTILIZAMOS EL MÉTODO CANVINOM, PASÁNDOLE EL INSITUTO ANTERIOR LEÍDO
		canviNom("28923792Z", "MarcGUAPO", institut);
		
		//DESPUÉS DE MODIFICAR EL INSTITUTO, ESCRIBIMOS EL RESULTADO EN EL FICHERO
		escriureXml(institut);
		
		//EJERCICIO3
		//LEEMOS XML Y ESCRIBIMOS EN JSON
		Alumne a = new Alumne();
		
		a = llegirXmlAlumne(); 
		
		escriureJSON(a);
		
		//LEEMOS JSON Y ESCRIBIMOS XML
		//NO ME HA DADO TIEMPO A TERMINARLO, POR ESO LO COMENTO
		/*a = llegirJSON();
		
		escriureXml(a);*/
	}
	
	private static void Serializar(ArrayList<Gat> gats)
	{
		
		try
		{
			FileOutputStream fos = new FileOutputStream("gat.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			for(Gat g : gats)
			{
				oos.writeObject(g);
			}
					
			
		}catch(Exception e)
		{
			//e.printStackTrace();
		}
	}
	
	private static ArrayList<String> llegirTxt() 
	{	 
		ArrayList<String> gats = new ArrayList<String>();
		
		try 
	      {
	         File f = new File ("gats.txt");
	         FileReader fr = new FileReader (f);
	         BufferedReader br = new BufferedReader(fr);
	         
	         while(br.ready())
	         {		         
		         gats.add(br.readLine() + "-" + br.readLine()  + "-" + br.readLine()  + "-" + br.readLine()  + "-" + br.readLine());
	         } 
	         
	         return gats;
	      }
	      catch(Exception e){
	         e.printStackTrace();
	      }	
		
		return gats;
		
	}

	private static Institut llegirXml()
	{
		Institut institut = null;
		
		try {  
			   
	        File file = new File("institut.xml");  
	        JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);  
	        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
	        institut= (Institut) jaxbUnmarshaller.unmarshal(file);  
	          	       
	        return institut;
	        
	      } catch (Exception e) {  
	        e.printStackTrace();  
	      }  
		
		return institut;
	}

	private static void canviNom(String DNI, String nomNou, Institut institut)
	{
		boolean trobat = false;
		
		for(Professor p : institut.getProfessors())
		{
			if(p.getDNI().equals(DNI) && trobat == false)
			{
				p.setNom(nomNou);
				trobat = true;
			}
		}
		
		if(trobat == false)
		{
			for(Alumne a : institut.getAlumnes())
			{
				if(a.getDNI().equals(DNI) && trobat == false)
				{
					a.setNom(nomNou);
					trobat = true;
				}
			}
		}
	}

	private static void escriureXml(Institut institut)
	{
		try {
			
			FileOutputStream fos = new FileOutputStream("institut.xml");
			JAXBContext contextObj = JAXBContext.newInstance(Institut.class);
			Marshaller marshallerObj = contextObj.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");			
			marshallerObj.marshal(institut, fos);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static Alumne llegirXmlAlumne()
	{
		Alumne alumne = null;
		
		try {  
			   
	        File file = new File("alumne.xml");  
	        JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);  
	        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
	        alumne= (Alumne) jaxbUnmarshaller.unmarshal(file);  
	          	       
	        return alumne;
	        
	      } catch (Exception e) {  
	        e.printStackTrace();  
	      }  	
		
		return alumne;
	}

	private static void escriureJSON(Alumne a)
	{

		JSONArray telefons = new JSONArray();
		JSONObject alumne = new JSONObject();
		JSONObject telefon = null;
		
		alumne.put("nom", a.getNom());
		alumne.put("DNI", a.getDNI());
		alumne.put("adreca", a.getAdreca());
		alumne.put("mail", a.getMail());
		
		for(String s : a.getTelefons())
		{
			telefon = new JSONObject();
			
			telefon.put("telefon", Long.parseLong(s));
			
			telefons.add(telefon);
		}
		
		alumne.put("telefons", telefons);
	
		 try {
			 
			FileWriter file = new FileWriter("alumnePRUEBAJSON.json");

			 file.write(alumne.toJSONString());
			 file.flush();
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static Alumne llegirJSON()
	{
		Alumne a = new Alumne();
		ArrayList<String> telefons = new ArrayList<String>();
		String telefon = "";
		
        try {
        
        	JSONParser parser = new JSONParser();

			Object obj = parser.parse(new FileReader("alumne.json"));
			
			JSONObject alumne = (JSONObject) obj;
			
			a.setNom((String)alumne.get("nom"));
			a.setCognoms((String)alumne.get("cognoms"));
			a.setDNI((String)alumne.get("DNI"));
			a.setAdreca((String)alumne.get("adreca"));
			a.setNom((String)alumne.get("nom"));
			
			JSONArray telefonsJSON = (JSONArray) alumne.get("telefons");
			
			for(Object o : telefonsJSON)
			{
				//NO ME HA DADO TIEMPO A TERMINARLO
				
				
				
			
			}		
			
			return a;
			
			}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return a;
	}
}
