package examenUF1.examenUF1;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder={"nom", "cognoms", "DNI" , "adreca", "telefons", "mail" })
public class Professor {

	private String nom;
	private String cognoms;
	private String DNI;
	private String adreca;
	private ArrayList<String> telefons = new ArrayList<String>();
	private String mail;
	
	public Professor(String nom, String cognoms, String dNI, String adreca, ArrayList<String> telefons, String mail) 
	{
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		DNI = dNI;
		this.adreca = adreca;
		this.telefons = telefons;
		this.mail = mail;
	}

	public Professor() 
	{
		super();
	}
	@XmlElement
	public String getCognoms() 
	{
		return cognoms;
	}

	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}

	@XmlElement
	public String getNom() 
	{
		return nom;
	}

	public void setNom(String nom) 
	{
		this.nom = nom;
	}

	@XmlElement
	public String getDNI() 
	{
		return DNI;
	}

	public void setDNI(String dNI) 
	{
		DNI = dNI;
	}

	@XmlElement
	public String getAdreca() 
	{
		return adreca;
	}

	public void setAdreca(String adreca) 
	{
		this.adreca = adreca;
	}

	@XmlElementWrapper(name = "telefons")
	@XmlElement(name = "telefon")
	public ArrayList<String> getTelefons() 
	{
		return telefons;
	}

	public void setTelefons(ArrayList<String> telefons) 
	{
		this.telefons = telefons;
	}

	@XmlElement
	public String getMail() 
	{
		return mail;
	}

	public void setMail(String mail) 
	{
		this.mail = mail;
	}
	
	
	
	
	
	
}
