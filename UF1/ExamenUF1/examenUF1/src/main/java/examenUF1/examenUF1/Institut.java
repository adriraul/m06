package examenUF1.examenUF1;

import java.util.ArrayList;
import javax.xml.bind.annotation.*;  

@XmlRootElement(name = "institut")
@XmlType(propOrder={"nom", "alumnes" , "professors" })
public class Institut {
	
	private String nom;
	private ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
	private ArrayList<Professor> professors = new ArrayList <Professor>();
	
	public Institut(String nom, ArrayList<Alumne> alumnes, ArrayList<Professor> professors) 
	{
		super();
		this.nom = nom;
		this.alumnes = alumnes;
		this.professors = professors;
	}
	
	public Institut() 
	{
		super();
	}
	
	@XmlElementWrapper(name = "professors")
	@XmlElement(name = "professor")
	public ArrayList<Professor> getProfessors() 
	{
		return professors;
	}

	public void setProfessors(ArrayList<Professor> professors) 
	{
		this.professors = professors;
	}

	@XmlElement
	public String getNom() 
	{
		return nom;
	}

	public void setNom(String nom) 
	{
		this.nom = nom;
	}
	
	@XmlElementWrapper(name = "alumnes")
	@XmlElement(name = "alumne")
	public ArrayList<Alumne> getAlumnes() 
	{
		return alumnes;
	}

	public void setAlumnes(ArrayList<Alumne> alumnes) 
	{
		this.alumnes = alumnes;
	}
	
	

	
	
}
