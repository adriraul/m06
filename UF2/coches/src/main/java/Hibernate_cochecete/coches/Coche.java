package Hibernate_cochecete.coches;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "coche")
public class Coche {
	
	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name = "marca")
	String marca;
	
	@Column(name = "modelo")
	String modelo;
	
	@Column(name = "combustible")
	String combustible;
	
	@Column(name = "potencia")
	int potencia;
	
	@Column(name = "cambioAuto")
	boolean cambioAuto;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getCombustible() {
		return combustible;
	}

	public void setCombustible(String combustible) {
		this.combustible = combustible;
	}

	public int getPotencia() {
		return potencia;
	}

	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}

	public boolean isCambioAuto() {
		return cambioAuto;
	}

	public void setCambioAuto(boolean cambioAuto) {
		this.cambioAuto = cambioAuto;
	}
}	
