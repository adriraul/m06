package Hibernate_cochecete.coches;

import java.util.List;
import java.util.Random;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class App 
{
	static Coche cochecete;
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry; 
	
	
	public static synchronized SessionFactory getSessionFactory() {
	    if ( sessionFactory == null ) {

	        // exception handling omitted for brevityaa

	        serviceRegistry = new StandardServiceRegistryBuilder()
	                .configure("hibernate.cfg.xml")
	                .build();

	        sessionFactory = new MetadataSources( serviceRegistry )
	                    .buildMetadata()
	                    .buildSessionFactory();
	    }
	    return sessionFactory;
	}
	
	
	public static void main(String[] args) {
		///abrimos sesion. Eso se hace siempre
		try {
			session = getSessionFactory().openSession();
			
			//abrimos PRIMERA transaccion. Eso se hace siempre.
			session.beginTransaction();
			
			Random r = new Random();
			
			for(int i = 200; i < 210; i++) 
			{
				Coche coche = new Coche();
				coche.setId(i);
				coche.setModelo("Modelo " + i);
				coche.setMarca("Marca" + i);
				coche.setPotencia((int)r.nextInt(500)+ 1);
				coche.setCambioAuto(r.nextBoolean());
				coche.setCombustible("JAJA");
				
				session.saveOrUpdate(coche);
			}
			
			session.getTransaction().commit();
			
			/*Coche cochecetecientouno = session.get(Coche.class, 101);
			
			System.out.println(cochecetecientouno.getModelo());
			
			cochecetecientouno.setMarca("Lamborgini");
			cochecetecientouno.setModelo("Murciélago");
			
			session.saveOrUpdate(cochecetecientouno);			
			
			session.getTransaction().commit();
			
	        session.beginTransaction();
	         
	         List<Object> todosloscoches = session.createQuery("FROM Coche").list();
	         
	         for (Object o : todosloscoches)
	         {
	            Coche c = (Coche)o;
	            
	            System.out.println("El coche "+ c.getMarca() +" tiene una potencia de "+c.getPotencia());
	         }
	         

	        session.getTransaction().commit();*/
			System.out.println("todo ha salido a pedir de Milhouse");
			
		} catch(Exception sqlException) 
		{
			sqlException.printStackTrace();
			
			if(null != session.getTransaction()) 
			{
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			sqlException.printStackTrace();
			
		} finally 
		{
			if(session != null) 
			{
				session.close();
			}
		}
	}
}
